package com.bigfans.userservice.service.impl;

import com.bigfans.framework.dao.BaseServiceImpl;
import com.bigfans.framework.utils.MD5;
import com.bigfans.userservice.dao.UserDAO;
import com.bigfans.userservice.model.User;
import com.bigfans.userservice.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;

/**
 * 用户服务类
 * @author lichong
 *
 */
@Service(UserServiceImpl.BEAN_NAME)
public class UserServiceImpl extends BaseServiceImpl<User> implements UserService {
	
	public static final String BEAN_NAME = "userService";
	
	private UserDAO userDAO;

	@Autowired
	private UserService _this;
	
	@Autowired
	public UserServiceImpl(UserDAO userDAO) {
		super(userDAO);
		this.userDAO = userDAO;
	}
	
	@Transactional(readOnly=true)
	public boolean exist(User user) throws Exception {
		int count = userDAO.countByAccount(user.getAccount());
		return count >= 1;
	}
	
	@Transactional
	public User regist(User user) throws Exception{
		user.setPassword(MD5.toMD5(user.getPassword()));
		super.create(user);
		return user;
	}

	@Transactional(readOnly=true)
	@Override
	public User login(String account, String password) throws Exception {
		return login(account , password , true);
	}
	
	@Override
	public User login(String account, String password, boolean md5) throws Exception {
		User example = new User();
		example.setAccount(account);
		if(md5){
			example.setPassword(MD5.toMD5(password));
		}else{
			example.setPassword(password);
		}
		User loginUser = load(example);
		return loginUser;
	}
	
	@Transactional
	@Override
	public User activate(User user) throws Exception{
		User dbUser = super.load(user);	
		//如果激活码不相同
		if(dbUser == null || !user.getActiveKey().equals(dbUser.getActiveKey())){
			throw new Exception("激活码不正确");
		}
		//如果已经激活
		if(dbUser.getStatus() == User.STATUS_ACTIVATED){
			throw new Exception("您已经激活该用户");
		}
		// 如果两天内没有激活
//		if(DateUtils.dayDiff(user.getRegDate(), dbUser.getRegDate()) > 2){
//			//如果超过激活有效期,则删除这条记录.
//			userMapper.delete(dbUser.getId());
//			throw new Exception("链接已经过期");
//		}
		update(user);
		return dbUser;
	}

	@Transactional(readOnly=true)
	@Override
	public boolean emailExist(String email) throws Exception {
		User userEntity = new User();
		userEntity.setEmail(email);
		Long count = super.count(userEntity);
		return count > 0;
	}
	
	@Transactional(readOnly=true)
	@Override
	public boolean accountExist(String account) throws Exception {
		User userEntity = new User();
		userEntity.setAccount(account);
		Long count = count(userEntity);
		return count > 0;
	}

	@Override
	@Transactional
	public void usePoints(String userId, Float points) throws Exception {
		int usedPoints = userDAO.usePoints(userId, points);
		if(usedPoints < 1){
			throw new Exception("使用积分失败");
		}
	}

	@Override
	@Transactional
	public void useBalance(String userId, BigDecimal balance) throws Exception {
		int usedBalance = userDAO.useBalance(userId, balance);
		if(usedBalance < 1){
			throw new Exception("使用余额失败");
		}
	}
}
