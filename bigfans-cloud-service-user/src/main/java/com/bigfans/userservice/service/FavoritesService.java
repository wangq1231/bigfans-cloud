package com.bigfans.userservice.service;

import com.bigfans.framework.dao.BaseService;
import com.bigfans.framework.model.PageBean;
import com.bigfans.userservice.model.Favorites;

/**
 * 
 * @Description:
 * @author lichong
 * @date Mar 10, 2016 3:25:03 PM
 *
 */
public interface FavoritesService extends BaseService<Favorites> {

	PageBean<Favorites> pageByUser(String userId, Long start, Long pagesize) throws Exception;
	
	Favorites getByUser(String userId, String prodId) throws Exception;
	
}
