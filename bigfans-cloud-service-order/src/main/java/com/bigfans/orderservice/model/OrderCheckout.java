package com.bigfans.orderservice.model;

import lombok.Data;

import java.math.BigDecimal;
import java.util.List;

/**
 * @author lichong
 * @create 2018-02-17 下午5:53
 **/
@Data
public class OrderCheckout {

    private BigDecimal totalPrice;
    private BigDecimal originalTotalPrice;
    private BigDecimal savedMoney;
    private List<OrderCheckoutItem> items;
    private Integer totalQuantity;

}
