package com.bigfans.model.event.payment;

import lombok.Data;

@Data
public class OrderPayFailureEvent {

    private String orderId;

    public OrderPayFailureEvent(String orderId) {
        this.orderId = orderId;
    }
}
