package com.bigfans.catalogservice.model.entity;

import com.bigfans.framework.model.AbstractModel;

import javax.persistence.Column;
import javax.persistence.Table;


/**
 * 
 * @Title: 
 * @Description: 品牌
 * @author lichong 
 * @date 2015年12月13日 下午6:04:46 
 * @version V1.0
 */
@Table(name="Brand")
public class BrandEntity extends AbstractModel {

	private static final long serialVersionUID = 6251503210742416953L;
	
	@Column(name="name")
	protected String name;
	@Column(name="logo")
	protected String logo;
	@Column(name="category_id")
	protected String categoryId;
	@Column(name="recommended")
	protected Boolean recommended;
	@Column(name="description")
	protected String description;
	@Column(name="order_num")
	protected Integer orderNum;
	
	public String getModule() {
		return "Brand";
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLogo() {
		return logo;
	}
	public void setLogo(String logo) {
		this.logo = logo;
	}

	public String getCategoryId() {
		return categoryId;
	}

	public void setCategoryId(String categoryId) {
		this.categoryId = categoryId;
	}

	public Boolean getRecommended() {
		return recommended;
	}

	public void setRecommended(Boolean recommended) {
		this.recommended = recommended;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Integer getOrderNum() {
		return orderNum;
	}

	public void setOrderNum(Integer orderNum) {
		this.orderNum = orderNum;
	}
}
