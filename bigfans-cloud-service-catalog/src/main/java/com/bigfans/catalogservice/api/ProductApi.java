package com.bigfans.catalogservice.api;

import com.bigfans.catalogservice.model.*;
import com.bigfans.catalogservice.service.category.CategoryService;
import com.bigfans.catalogservice.service.product.ProductImageService;
import com.bigfans.catalogservice.service.product.ProductService;
import com.bigfans.catalogservice.service.productgroup.ProductGroupService;
import com.bigfans.catalogservice.service.spec.SpecService;
import com.bigfans.framework.web.RestResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ProductApi {

	@Autowired
	private ProductService productService;
	@Autowired
	private CategoryService categoryService;
	@Autowired
	private ProductImageService productImageService;
	@Autowired
	private ProductGroupService productGroupService;
	@Autowired
	private SpecService specService;

	@GetMapping(value = "/product/{id}")
	public RestResponse detail(@PathVariable("id") String pid) throws Exception {

		// 商品基本信息
		Product product = productService.load(pid);

		// 规格信息
		List<ProductSpec> specs = specService.listProductSpecs(pid);
		product.setSpecList(specs);

		return RestResponse.ok(product);
	}
}
