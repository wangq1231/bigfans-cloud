package com.bigfans.searchservice.model.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @Title: 
 * @Description: 
 * @author lichong 
 * @date 2016年3月26日 上午10:07:59 
 * @version V1.0
 */
@Data
public class UnSelectedFilter {
	
	private String label;
	private List<SearchFilterItem> items = new ArrayList<>();
	
	public UnSelectedFilter(String label) {
		this.label = label;
	}

	public void addItem(SearchFilterItem item){
		this.items.add(item);
	}
	
	public boolean hasSelectedItems(){
		return items.size() > 0;
	}
}
