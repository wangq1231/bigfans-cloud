package com.bigfans.searchservice.dao.impl;

import com.bigfans.framework.dao.MybatisDAOImpl;
import com.bigfans.searchservice.dao.BrandDAO;
import com.bigfans.searchservice.model.Brand;
import org.springframework.stereotype.Repository;

@Repository(BrandDAOImpl.BEAN_NAME)
public class BrandDAOImpl extends MybatisDAOImpl<Brand> implements BrandDAO {

	public static final String BEAN_NAME = "brandDAO";
	
}
