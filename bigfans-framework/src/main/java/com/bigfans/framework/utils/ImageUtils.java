package com.bigfans.framework.utils;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Hashtable;
import java.util.Map;

import javax.imageio.ImageIO;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.EncodeHintType;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel;

import net.coobird.thumbnailator.Thumbnails;

/**
 * 
 * @Description:图片工具
 * @author lichong
 * 2015年1月5日上午8:50:28
 *
 */
public class ImageUtils {
	
	private static final int BLACK = 0xFF000000;
	private static final int WHITE = 0xFFFFFFFF;

	private static BufferedImage toBufferedImage(BitMatrix matrix) {
		int width = matrix.getWidth();
		int height = matrix.getHeight();
		BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
		for (int x = 0; x < width; x++) {
			for (int y = 0; y < height; y++) {
				image.setRGB(x, y, matrix.get(x, y) ? BLACK : WHITE);
			}
		}
		return image;
	}

	private static void writeToFile(BitMatrix matrix, String format, File file) throws IOException {
		BufferedImage image = toBufferedImage(matrix);
		if (!ImageIO.write(image, format, file)) {
			throw new IOException("Could not write an image of format " + format + " to " + file);
		}
	}

	/**
	 * 将内容contents生成长宽均为width的图片，图片路径由imgPath指定
	 */
	public static File getQRCodeImge(String contents, int width, String imgPath) {
		return getQRCodeImge(contents, width, width, imgPath);
	}

	/**
	 * 将内容contents生成长为width，宽为width的图片，图片路径由imgPath指定
	 */
	public static File getQRCodeImge(String contents, int width, int height, String imgPath) {
		try {
			Map<EncodeHintType, Object> hints = new Hashtable<EncodeHintType, Object>();
			hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M);
			hints.put(EncodeHintType.CHARACTER_SET, "UTF8");

			BitMatrix bitMatrix = new MultiFormatWriter().encode(contents, BarcodeFormat.QR_CODE, width, height, hints);

			File imageFile = new File(imgPath);
			writeToFile(bitMatrix, "png", imageFile);

			return imageFile;

		} catch (Exception e) {
			return null;
		}
	}

	public static BufferedImage compress(InputStream image , int weidth , int height) throws IOException{
		return Thumbnails.of(image).size(weidth, height).asBufferedImage();
	}
	
	public static void compress(File src ,File dest, int weidth , int height) throws IOException{
		InputStream is = new FileInputStream(src);
		Thumbnails.of(is).size(weidth, height).toFile(dest);
	}
	
	public static void compress(InputStream is ,File dest, int weidth , int height) throws IOException{
		Thumbnails.of(is).size(weidth, height).toFile(dest);
	}
}
