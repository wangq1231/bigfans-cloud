package com.bigfans.framework.cache;

import java.util.List;

/**
 * 
 * @Description: 
 * @author lichong
 * @date Mar 2, 2016 8:14:13 PM 
 *
 */
public class EhCacheProvider implements CacheProvider {

	@Override
	public CacheEntry get(CacheKey key) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void put(CacheEntry value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void update(CacheEntry value) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void evict(CacheKey key) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void evict(List<CacheKey> keys) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void clear() {
		// TODO Auto-generated method stub
		
	}
	
	public void shutdown(){
	}

//	private static CacheManager ehcacheManager;
//	
//	public EhCacheProvider() {
//		String configPath = Configuration.getProp("cache.ehcache_config_location");
//		URL url = EhCacheProvider.class.getResource(configPath);
//		ehcacheManager = CacheManager.create(url);
//	}
//	
//	public void shutdown(){
//		if(ehcacheManager != null){
//			ehcacheManager.shutdown();
//		}
//	}
//
//	@Override
//	public CacheEntry get(CacheKey key) {
//		Cache cache = ehcacheManager.getCache("dbcache");
//		Element element = cache.get(key.getKey());
//		if(element == null){
//			return null;
//		}
//		Object obj = element.getObjectValue();
//		if(obj != null){
//			return new CacheEntry(key,element.getObjectValue());
//		}
//		return null;
//	}
//
//	@Override
//	public void put(CacheEntry cacheEntry) {
//		Cache cache = ehcacheManager.getCache("dbcache");
//		Element element = new Element(cacheEntry.getKey().getKey() , cacheEntry.getValue());
//		cache.put(element);
//	}
//
//	@Override
//	public void update(CacheEntry cacheEntry) {
//		this.evict(cacheEntry.getKey());
//		this.put(cacheEntry);
//	}
//
//	@Override
//	public void evict(CacheKey key) {
//		Cache cache = ehcacheManager.getCache("dbcache");
//		cache.remove(key.getKey());
//	}
//
//	@Override
//	public void evict(List<CacheKey> keys) {
//		Cache cache = ehcacheManager.getCache("dbcache");
//		for (CacheKey cacheKey : keys) {
//			cache.remove(cacheKey.getKey());
//		}
//	}
//
//	@Override
//	public void clear() {
//		Cache cache = ehcacheManager.getCache("dbcache");
//		cache.removeAll();
//	}

}
