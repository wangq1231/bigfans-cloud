package com.bigfans.framework.jms;

import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

public interface MessageCreator {

	Message createMessage(Session session) throws JMSException;
	
}
